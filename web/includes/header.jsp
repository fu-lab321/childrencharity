<%-- 
    Document   : header
    Created on : Jan 23, 2021, 5:50:23 PM
    Author     : Hau Nguyen
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Quỹ Thiện Tâm</title>

        <link href="//fonts.googleapis.com/css2?family=DM+Sans:wght@400;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/style-starter.css">
    </head>
    <body>
        <!--header-->
        <header id="site-header" class="fixed-top">
            <div class="container">
                <nav class="navbar navbar-expand-lg stroke">
                    <h1><a class="navbar-brand mr-lg-5" href="home.jsp">
                            <img src="assets/images/logo.png" alt="Your logo" title="Your logo" />Quỹ Thiện Tâm
                        </a></h1>
                    <!-- if logo is image enable this   
                  <a class="navbar-brand" href="#index.html">
                      <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
                  </a> -->
                    <button class="navbar-toggler  collapsed bg-gradient" type="button" data-toggle="collapse"
                            data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon fa icon-expand fa-bars"></span>
                        <span class="navbar-toggler-icon fa icon-close fa-times"></span>
                        </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav w-100">
                            <li class="nav-item ${ActiveHome}">
                                <a class="nav-link" href="introduceHome">Trang Chủ<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item ${ActiveAbout}">
                                <a class="nav-link" href="about">Giới Thiệu</a>
                            </li>
                            <li class="nav-item ${ActiveCause}">
                                <a class="nav-link" href="cause">Gây Quỹ</a>
                            </li>
                            <li class="nav-item ${ActiveContact}">
                                <a class="nav-link" href="contact">Liên Hệ</a>
                            </li>
                            <li class="ml-lg-auto mr-lg-0 m-auto">
                                <!--/search-right-->
                                <div class="search-right">
                                    <a href="#search" title="search"><span class="fa fa-search" aria-hidden="true"></span></a>
                                    <!-- search popup -->
                                    <div id="search" class="pop-overlay">
                                        <div class="popup">
                                            <h4 class="mb-3">Tìm Kiếm</h4>
                                            <form action="error.html" method="GET" class="search-box">
                                                <input type="search" placeholder="Nhập Kí Tự" name="search" required="required"
                                                       autofocus="">
                                                <button type="submit" class="btn btn-style btn-primary">Tìm Kiếm</button>
                                            </form>

                                        </div>
                                        <a class="close" href="#close">×</a>
                                    </div>
                                    <!-- /search popup -->
                                </div>
                                <!--//search-right-->
                            </li>
                            <li class="align-self">
                                <a href="login.jsp" class="btn btn-style btn-primary ml-lg-3 mr-lg-2"><span class="fa fa-heart mr-1"></span> Chung Tay</a>
                            </li>         

                        </ul>
                    </div>
                    <!-- toggle switch for light and dark theme -->
                    <div class="mobile-position">
                        <nav class="navigation">
                            <div class="theme-switch-wrapper">
                                <label class="theme-switch" for="checkbox">
                                    <input type="checkbox" id="checkbox">
                                    <div class="mode-container">
                                        <i class="gg-sun"></i>
                                        <i class="gg-moon"></i>
                                    </div>
                                </label>
                            </div>
                        </nav>
                    </div>
                    <!-- //toggle switch for light and dark theme -->
                </nav>
            </div>
        </header>
