<%-- 
    Document   : adminHeader
    Created on : Feb 15, 2021, 4:51:30 PM
    Author     : DELL-PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "s" uri = "/struts-tags" %>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Quản Trị Viên</title>
    <!-- plugins:css -->
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/shared/style.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/demo_1/style.css">
    <!-- End Layout styles -->
    <link rel="shortcut icon" href="assets/images/favicon.ico" />
    <link rel="stylesheet" href="assets/css/style-starter.css">
</head>
<body>
    <div class="container-scroller">
        <!-- partial:../../partials/_navbar.html -->
        <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
                <a class="navbar-brand brand-logo" href="">

                    <a class="navbar-brand brand-logo-mini" href="">
                        <img src="" alt="" /> </a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center">
                <ul class="navbar-nav">
                    <li class="nav-item font-weight-semibold d-none d-lg-block">Giúp Đỡ : +050 2992 709</li>
                    <li class="nav-item dropdown language-dropdown">
                        <a class="nav-link dropdown-toggle px-2 d-flex align-items-center" id="LanguageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                            <div class="d-inline-flex mr-0 mr-md-3">
                                <div class="flag-icon-holder">
                                    <i class="flag-icon flag-icon-us"></i>
                                </div>
                            </div>
                            <span class="profile-text font-weight-medium d-none d-md-block">Gmail: quythientam@gmail.com</span>
                        </a>
                    </li>
                </ul>
                <form class="ml-auto search-form d-none d-md-block" action="#">
                    <div class="form-group">
                        <input type="search" class="form-control" placeholder="Tìm Kiếm">
                    </div>
                </form>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
                        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                            <img class="img-xs rounded-circle" src="images/quythientam.png" alt="Profile image"> </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                            <div class="dropdown-header text-center">
                                <img class="img-md rounded-circle" src="images/quythientam.png" alt="Profile image">
                                <p class="mb-1 mt-3 font-weight-semibold">Quỹ Thiện Tâm</p>
                                <p class="font-weight-light text-muted mb-0">quythientam@gmail.com</p>
                            </div>
                            <a class="dropdown-item" href="editAdmin">Sửa Thông Tin<i class="dropdown-item-icon ti-comment-alt"></i></a>
                            <a class="dropdown-item" href="changePassAdmin.jsp">Đổi Mật Khẩu<i class="dropdown-item-icon ti-comment-alt"></i></a>
                            <a class="dropdown-item" href="logoutAction">Thoát<i class="dropdown-item-icon ti-location-arrow"></i></a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
        <div class="container-fluid page-body-wrapper">
            <!-- partial:../../partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item nav-profile">
                        <a href="#" class="nav-link">
                            <div class="profile-image">
                                <img class="img-xs rounded-circle" src="images/quythientam.png" alt="profile image">
                                <div class="dot-indicator bg-Fccess"></div>
                            </div>
                            <div class="text-wrapper">
                                <p class="profile-name">Quỹ Thiện Tâm</p>
                                <p class="designation">Quản Lí</p>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item nav-category">Menu Chính</li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                            <i class="menu-icon typcn typcn-coffee"></i>
                            <span class="menu-title">Chiến Dịch</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="ui-basic">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="viewCampaignAdmin">Danh Sách Chiến Dịch</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="addCampaignAdmin.jsp">Thêm Chiến Dịch</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    
<!--                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                            <i class="menu-icon typcn typcn-document-add"></i>
                            <span class="menu-title">Kiểm Tra Quyên Góp</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="auth">
                            <ul class="nav flex-column sub-menu">
                                <s:iterator status="stat" value="camss">
                                <li class="nav-item">
                                    <a class="nav-link" href="viewDonatesByIdCampaign?idCampaign=<s:property value="idCampaign"></s:property>"> <s:property value="name"></s:property> </a>
                                </li>
                                </s:iterator>
                            </ul>
                        </div>
                    </li>-->
                    
                    <li class="nav-item">
                        <a class="nav-link" href="viewUser">
                            <i class="menu-icon typcn typcn-shopping-bag"></i>
                            <span class="menu-title">Thông Tin Người Dùng</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="viewVolunteerAdmin">
                            <i class="menu-icon typcn typcn-shopping-bag"></i>
                            <span class="menu-title">Danh sách Tình Nguyện Viên</span>
                        </a>
                    </li>
                    
                </ul>
            </nav>
