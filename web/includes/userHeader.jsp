<%-- 
    Document   : userHeader
    Created on : Feb 27, 2021, 1:23:09 AM
    Author     : DELL-PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="assets/css/shared/style.css">
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/demo_1/style.css">
        <!-- End Layout styles -->
        <link rel="shortcut icon" href="assets/images/favicon.ico" />
        <link rel="stylesheet" href="assets/css/style-starter.css">

        <title>Quỹ Thiện Tâm</title>

        <link href="//fonts.googleapis.com/css2?family=DM+Sans:wght@400;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/style-starter.css">
    </head>
    <body>
        <!--header-->
        <header id="site-header" class="fixed-top">
            <div class="container">
                <nav class="navbar navbar-expand-lg stroke">
                    <h1><a class="navbar-brand mr-lg-5" href="userHome.jsp">
                            <img src="assets/images/logo.png" alt="Your logo" title="Your logo" />Quỹ Thiện Tâm
                        </a></h1>
                    <!-- if logo is image enable this   
                  <a class="navbar-brand" href="#index.html">
                      <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
                  </a> -->
                    <button class="navbar-toggler  collapsed bg-gradient" type="button" data-toggle="collapse"
                            data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon fa icon-expand fa-bars"></span>
                        <span class="navbar-toggler-icon fa icon-close fa-times"></span>
                        </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav w-100">
                            <li class="nav-item ${ActiveHome}">
                                <a class="nav-link" href="userIntroduceHome">Trang Chủ<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item ${ActiveAbout}">
                                <a class="nav-link" href="aboutUser">Giới Thiệu</a>
                            </li>
                            <li class="nav-item ${ActiveCause}">
                                <a class="nav-link" href="Usercause">Gây Quỹ</a>
                            </li>
                            
                            <li class="nav-item ${ActiveContact}">
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                </div>
                                <a class="nav-link" href="viewCart">Gifts Cart <i class="fa fa-gift"></i></a>
                            </li>
                            
                            <li class="nav-item ${ActiveContact}">
                                <a class="nav-link" href="contactUser">Liên Hệ</a>
                            </li>

                            <li class="ml-lg-auto mr-lg-0 m-auto">
                                <!--/search-right-->
                                <div class="search-right">
                                    <a href="#search" title="search"><span class="fa fa-search" aria-hidden="true"></span></a>
                                    <!-- search popup -->
                                    <div id="search" class="pop-overlay">
                                        <div class="popup">
                                            <h4 class="mb-3">Tìm Kiếm</h4>
                                            <form action="error.html" method="GET" class="search-box">
                                                <input type="search" placeholder="Nhập Kí Tự" name="search" required="required"
                                                       autofocus="">
                                                <button type="submit" class="btn btn-style btn-primary">Tìm Kiếm</button>
                                            </form>

                                        </div>
                                        <a class="close" href="#close">×</a>
                                    </div>
                                    <!-- /search popup -->
                                </div>
                                <!--//search-right-->
                            </li>

                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
                                    <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                                        <img class="img-xs rounded-circle" src="images/avatar.png" alt="Profile image"> </a>
                                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                                        <div class="dropdown-header text-center">
                                            <img class="img-md rounded-circle" src="images/avatar.png" alt="Profile image">
                                            <p class="mb-1 mt-3 font-weight-semibold">${sessionScope.USER.name}</p>
                                            <p class="font-weight-light text-muted mb-0">${sessionScope.USER.gmail}</p>
                                        </div>
                                        <a class="dropdown-item" href="editUserInfor">Sửa Thông Tin<i class="dropdown-item-icon ti-comment-alt"></i></a>
                                        <a class="dropdown-item" href="userHistoryDonates">Lịch sử Donates<i class="dropdown-item-icon ti-comment-alt"></i></a>                                
                                        <a class="dropdown-item" href="userChangePassword.jsp">Đổi Mật Khẩu<i class="dropdown-item-icon ti-comment-alt"></i></a>
                                        <a class="dropdown-item" href="logoutAction">Đăng Xuất<i class="dropdown-item-icon ti-location-arrow"></i></a>
                                    </div>
                                </li>
                            </ul>

                        </ul>
                    </div>
                    <!-- toggle switch for light and dark theme -->
                    <div class="mobile-position">
                        <nav class="navigation">
                            <div class="theme-switch-wrapper">
                                <label class="theme-switch" for="checkbox">
                                    <input type="checkbox" id="checkbox">
                                    <div class="mode-container">
                                        <i class="gg-sun"></i>
                                        <i class="gg-moon"></i>
                                    </div>
                                </label>
                            </div>
                        </nav>
                    </div>
                    <!-- //toggle switch for light and dark theme -->
                </nav>
            </div>
        </header>
